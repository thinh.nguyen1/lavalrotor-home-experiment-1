"""Functions for processing measurement data"""

import numpy as np
from numpy.fft import fft
from typing import Tuple


def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """
    # Bilden des Betrags
    
    betrag = np.sqrt(x**2 + y**2 + z**2)
    
    return(betrag)

def interpolation(time: np.ndarray, data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """

    aequidistante_zeit = np.linspace(np.min(time), np.max(time), len(time))
    
    data_interpolation = np.interp(aequidistante_zeit, time, data)
    
    return aequidistante_zeit, data_interpolation


def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
    
    # Mittelwert 
    
    x -= np.mean(x)
    
    # Berechnung Fast-Fourier-Transformation
    
    fft_ergebnis = fft(x)
    
    sampling_rate = 1/(time[1] - time[0])
    frequenzen = np.fft.fftfreq(len(x), d = 1/sampling_rate)
    
    # da negative Frequenzen in diesem Fall nicht von Relevanz sind, werden diese nicht betrachtet
    
    positive_frequenzen = frequenzen >= 0
    
    return np.abs(fft_ergebnis[positive_frequenzen]), frequenzen[positive_frequenzen]


