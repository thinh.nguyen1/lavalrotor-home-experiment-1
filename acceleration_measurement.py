import time
import h5py
import adafruit_adxl34x
import numpy as np
import board
import json
import os

from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting

"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start

# TODO: Adjust the parameters to your needs

path_setup_json = "datasheets/setup_handy.json"  # adjust this to the setup json path

#path_setup_json2 = "datasheets/setup_subwoofer.json"

measure_duration_in_s = 20

# ---------------------------------------------------------------------------------------------#1-end

"""Prepare Metadata and create H5-File"""
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="datasheets")

print("Setup dictionary:")
print(json.dumps(setup_json_dict, indent=2, default=str))
print()
print("Sensor settings dictionary")
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


"""Establishing a connection to the acceleration sensor"""
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)


# -------------------------------------------------------------------------------------------#2-start

# TODO: Initialize the data structure

uuid = "1ee847be-fddd-6ee4-892a-68c4555b0981"

dictionary_data = {
    uuid: {
        "acceleration_x": [],
        "acceleration_y": [],
        "acceleration_z": [],
        "timestamp": []
    }
}        

# ---------------------------------------------------------------------------------------------#2-end


# -------------------------------------------------------------------------------------------#3-start

# TODO: Measure the probe

#import time
#import board
#import adafruit_adxl34x

i2c = board.I2C()  # uses board.SCL and board.SDA

accelerometer = adafruit_adxl34x.ADXL345(i2c)

start = time.time()

while time.time() - start < measure_duration_in_s:
    acceleration = accelerometer.acceleration
    timestamp = time.time() - start
    
    dictionary_data[uuid]["acceleration_x"].append(acceleration[0])   
    dictionary_data[uuid]["acceleration_y"].append(acceleration[1])
    dictionary_data[uuid]["acceleration_z"].append(acceleration[2])
    dictionary_data[uuid]["timestamp"].append(timestamp)
    
    time.sleep(0.001) # 1 Millisekunde    

# ---------------------------------------------------------------------------------------------#3-end

# -------------------------------------------------------------------------------------------#4-start

# TODO: Write results in hdf5

hdf5_Pfad = path_h5_file

with h5py.File(hdf5_Pfad, "a") as file:
    grp = file.create_group("RawData")    
    subgrp = grp.create_group(uuid)
    
    data_acc_x = subgrp.create_dataset("acceleration_x", data = dictionary_data[uuid]["acceleration_x"])
    
    data_acc_y = subgrp.create_dataset("acceleration_y", data = dictionary_data[uuid]["acceleration_y"])
    
    data_acc_z = subgrp.create_dataset("acceleration_z", data = dictionary_data[uuid]["acceleration_z"])
    
    timestamp = subgrp.create_dataset("timestamp", data = dictionary_data[uuid]["timestamp"])
    
    units = ["m/s^2", "m/s^2","m/s^2", "s"]
    i = 0
    for key in dictionary_data[uuid].keys():
        dataset = subgrp.create_dataset(key, data = dictionary_data[uuid][key])
        dataset.attrs["unit"] = units[i]
        i = i+1
    
file.close()

# ---------------------------------------------------------------------------------------------#4-end

"""Log JSON metadata"""
log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
print("Measurement data was saved in {}/".format(path_measurement_folder))
